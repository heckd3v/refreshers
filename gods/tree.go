package main

import (
	"fmt"
)

type TreeOps interface {
	Lookup(currNode *Node,value int) (bool,int)
}

type Node struct {
	Data int
	Left *Node
	Right *Node
}

type Tree struct{
	Head *Node
}

func (tree *Tree) Lookup(currNode *Node,value interface{}) (bool,int){
	if currNode==nil {
		fmt.Println("Not a valid node to traverse the tree")
		return false,-1		
	}
	return false,-1
}
func main() {
	tree:=&Tree{}
	tree.Lookup(tree.Head,1)
}