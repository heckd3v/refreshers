#!/bin/sh
goMainFile=$1
kill -9 $(ps -e | grep 'go run '$goMainFile'.go' | awk '{print $1}')
