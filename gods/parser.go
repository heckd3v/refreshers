package parser

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
	"strings"
)

func ParseStdint() int64 {
	reader :=bufio.NewReader(os.Stdin)
	sNum,_ :=reader.ReadString('\n')
	sNum =strings.TrimSpace(sNum)
	num,_ :=strconv.ParseInt(sNum,10,64)
	return num
}
