package main

import (
	"errors"
	"fmt"
)

type DA struct {
	currentIndex int
	data         []interface{}
}
type DAapi interface {
	GetSize() int
	GetItem(index int) (interface{}, error)
	PutItem(item interface{})
	ensureCapacity(minimumCapacity int)
}

func (da *DA) GetSize() int {
	return len(da.data)
}
func (da *DA) GetItem(index int) (interface{}, error) {
	if index > len(da.data) {
		err := errors.New("Array index out of bound exception")
		return nil, err
	}
	return da.data[index], nil
}

func (da *DA) PutItem(item interface{}) {
	da.ensureCapacity(da.currentIndex + 1)
	da.data[da.currentIndex] = item
	da.currentIndex += 1
}

func (da *DA) ensureCapacity(minimumCapacity int) {
	currentCapacity := da.GetSize()
	fmt.Println("***********************************")
	fmt.Println(fmt.Sprintf("Current size : %d", currentCapacity))
	if currentCapacity < minimumCapacity {
		desiredCapacity := currentCapacity * 2
		if desiredCapacity < minimumCapacity {
			desiredCapacity = minimumCapacity
		}
		newData := make([]interface{}, desiredCapacity)
		copy(newData, da.data)
		da.data = newData

		fmt.Println(fmt.Sprintf("Minimum Capacity: %d\nDesired Capacity : %d\nNew Array size : %d", minimumCapacity, desiredCapacity, da.GetSize()))
	} else {
		fmt.Println(fmt.Sprintf("Minimum Capacity: %d, Not Increasing the capacity", minimumCapacity))
	}
	fmt.Println("-----------------------------------")
}

func NewDA() *DA {
	da := DA{}
	da.currentIndex = 0
	return &da
}

func main() {
	dynarray := NewDA()
	dynarray.PutItem(10)
	dynarray.PutItem(12)
	dynarray.PutItem(13)
	dynarray.PutItem(14)
	dynarray.PutItem(15)
	dynarray.PutItem(16)
	dynarray.PutItem(16)
	dynarray.PutItem(16)
	dynarray.PutItem(16)
	fmt.Println(fmt.Sprintf("Size is %d", dynarray.GetSize()))
}
