package main

import (
	"fmt"
)

type OnewayNode struct {
	value interface{}
	next  *OnewayNode
}

type OnewayLinkedListOp interface {
	Display()
	Put(value interface{})
	Delete()
}

type OnewayLinkedList struct {
	head *OnewayNode
}

//deleting from headside
func (node *OnewayLinkedList) Delete() {
	node.head = node.head.next
}

//addition is always at head
func (node *OnewayLinkedList) Put(value interface{}) {
	fmt.Println(fmt.Sprintf("Putting value %v", value))
	//construct a node
	incomingNode := OnewayNode{}
	incomingNode.value = value
	if node.head == nil {
		//incoming node is head
		node.head = &incomingNode
	} else {
		incomingNode.next = node.head
		node.head = &incomingNode
	}
}

func (node *OnewayLinkedList) Display() {
	fmt.Println("Inside Display")
	itr := node.head
	fmt.Println("**************")
	fmt.Print("(")
	for itr != nil {
		fmt.Print(fmt.Sprintf("%v ", itr.value))
		itr = itr.next
	}
	fmt.Print(")")
	fmt.Println()
	fmt.Println("--------------")
}

func main() {
	owl := OnewayLinkedList{}
	owl.Put(10)
	owl.Put(11)
	owl.Put(12)
	owl.Put(323)
	owl.Display()
	owl.Delete()
	owl.Display()
}
