package main

import (
	"fmt"
)

type matrix struct {
	row  int
	col  int
	data [][]interface{}
}

type matrixops interface {
	Init(row, col int)
	Transpose()
	RotateLeft()
	RotateRight()
}

func (mat *matrix) Init(row, col int) {
	mat.row = row
	mat.col = col
	mat.data = make([][]interface{}, row)
	for i := 0; i < mat.row; i++ {
		mat.data[i] = make([]interface{}, mat.col)
	}
}

func main() {
	matdata := matrix{}
	matdata.Init(2, 4)
	fmt.Println(fmt.Sprintf("%v", matdata))
	for i := 0; i < matdata.row; i++ {
		for j := 0; j < matdata.col; j++ {
			matdata.data[i][j] = (i*2 + j*3)
		}
	}
	fmt.Println(fmt.Sprintf("%v", matdata))
}
