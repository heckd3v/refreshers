package main

import (
	"flag"
	"fmt"
)

var (
	n int64
)

//With break at n=92, need math/big package to use binInt for sotring higher order fib numbers
func fibonaciRecursiveWithMemoization(n int64, memo *[]int64) int64 {
	if (n==0 || n==1) { return n }
	for j:=int64(2);j<=n;j++{ 
		if ((*memo)[j]==0) {
			(*memo)[j] = fibonaciRecursiveWithMemoization(j-1,memo)+fibonaciRecursiveWithMemoization(j-2,memo)		
		}
	}
	return (*memo)[n]
}

func main() {	
	flag.Int64Var(&n,"n",int64(0),"n : nth fibonaci number")
	flag.Parse()
	memo:=make([]int64,n+1)
	fmt.Println(fmt.Sprintf("Fib of %v = %v",n,fibonaciRecursiveWithMemoization(n,&memo)))
	
}