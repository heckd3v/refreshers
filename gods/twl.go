package main

import (
	"fmt"
)

type TwowayNode struct {
	value    interface{}
	next     *TwowayNode
	previous *TwowayNode
}

//TwowayListOp - Operations permitted on a TwowayList
type TwowayListOp interface {
	//Put - Insert the value at begining of list
	Put(value interface{})
	//Delete - Deletes the node at head
	Delete()
	//Display - Displays the content of list
	Display()
}

type TwowayList struct {
	head *TwowayNode
}

func (list *TwowayList) Put(value interface{}) {
	fmt.Println(fmt.Sprintf("Putting value %v at head", value))
	incomingNode := TwowayNode{}
	incomingNode.value = value
	if list.head == nil {
		list.head = &incomingNode
	} else {
		list.head.previous = &incomingNode
		incomingNode.next = list.head
		list.head = &incomingNode
	}
}

func (list *TwowayList) Delete() {
	fmt.Println(fmt.Sprintf("Delete head node"))
	list.head = list.head.next
	list.head.previous = nil
}

func (list *TwowayList) Display() {
	fmt.Println("Displaying the list")
	currNode := list.head
	fmt.Println("***********")
	fmt.Println()
	fmt.Print("(")
	for currNode != nil {
		fmt.Print(fmt.Sprintf("%v ", currNode.value))
		currNode = currNode.next
	}
	fmt.Print(")")
	fmt.Println()
	fmt.Println("----------")
}

func main() {
	twl := TwowayList{}
	twl.Put(1)
	twl.Put(2)
	twl.Put(3)
	twl.Display()
	twl.Delete()
	twl.Display()
}
