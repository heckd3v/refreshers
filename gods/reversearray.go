package main

import (
	"fmt"
)

var (
	intrfArray []interface{}
)

func reverse(arry []interface{}) {
	i := 0
	j := len(arry) - 1
	for i < j {
		temp := arry[i]
		arry[i] = arry[j]
		arry[j] = temp
		i++
		j--
	}
}
func main() {
	for i := 0; i < 10; i += 1 {
		intrfArray = append(intrfArray, i)
	}
	fmt.Println(fmt.Sprintf("Array before reversion :: %v ", intrfArray))
	reverse(intrfArray)
	fmt.Println(fmt.Sprintf("Array after reversion :: %v ", intrfArray))
}
