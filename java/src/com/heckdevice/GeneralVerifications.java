package com.heckdevice;

import com.heckdevice.dsa.udemy.DynamicArray;

/**
 * Created by shailesh on 24/07/17.
 */
public class GeneralVerifications {
    public static void main(String[] args) {
        //DynamicArrays tests
        DynamicArray<Integer> da = new DynamicArray<Integer>(1);
        for (int i = 0; i < 1025; i++) {
            da.put(i);
        }
        System.out.printf("Size is %d\n", da.getSize());
        int newCapacity = 129;
        System.out.printf("%d entries will result into %d size", newCapacity, (int) Math.pow(2, (int) Math.ceil(Math.log10(newCapacity) / Math.log10(2))));
    }
}
