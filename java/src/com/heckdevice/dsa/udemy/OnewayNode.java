package com.heckdevice.dsa.udemy;

/**
 * Created by shailesh on 26/07/17.
 */
public class OnewayNode<T> {
    T value;
    OnewayNode<T> next;

    public OnewayNode(T value, OnewayNode<T> next) {
        this.value = value;
        this.next = next;
    }
}
