package com.heckdevice.dsa.udemy;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by shailesh on 23/07/17.
 */
public class DynamicArrayTests {

    @Test
    public void TestArrayBoundsCheck() {
        DynamicArray<Integer> da = new DynamicArray<Integer>(0);
        try {
            da.getElement(2);
        } catch (Exception e) {
            Assert.assertEquals(e.getClass(), ArrayIndexOutOfBoundsException.class);
        }
    }

    @Test
    public void TestMinimumArraySize() {
        DynamicArray<Integer> da = new DynamicArray<>(0);
        Assert.assertEquals(1, da.getSize());
    }

    @Test
    public void TestArrayGrowthByTwo() {
        DynamicArray<Integer> da = new DynamicArray<>(0);
        da.put(10);
        da.put(11);
        da.put(12);
        da.put(13);
        da.put(14);
        Assert.assertEquals(8, da.getSize());
    }

    @Test
    public void TestGetItem() {
        DynamicArray<String> da = new DynamicArray<String>(0);
        da.put("11");
        Assert.assertEquals("11", da.getElement(0));

    }
}