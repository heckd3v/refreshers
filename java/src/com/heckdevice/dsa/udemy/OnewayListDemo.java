package com.heckdevice.dsa.udemy;

/**
 * Created by shailesh on 26/07/17.
 */
public class OnewayListDemo {
    public static void main(String[] args) {
        //LinkedList tests
        OnewayList<String> mystringLL = new OnewayList<String>();
        mystringLL.addToHead("B");
        mystringLL.addToHead("C");
        mystringLL.addToHead("C++");
        mystringLL.addToHead("Java");
        mystringLL.addToHead("GoLang");
        mystringLL.display();
        mystringLL.deleteHead();
        mystringLL.display();
    }
}
