package com.heckdevice.dsa.udemy;

/**
 * Created by shailesh on 30/07/17.
 */
public class TwowayList<T> {
    TwowayNode<T> head = null;

    public TwowayList() {
        this.head = null;
    }

    public void addToHead(TwowayNode<T> incomingNode){
        if(head==null){
            this.head=incomingNode;
        }
    }
}
