package com.heckdevice.dsa.udemy;

/**
 * Created by shailesh on 26/07/17.
 */
public class OnewayList<T> {
    OnewayNode<T> head;

    public OnewayList() {
        this.head = null;
    }

    public void addToHead(T value) {
        OnewayNode<T> incomingOnewayNode = new OnewayNode<T>(value, null);
        if (this.head == null) {
            //the incoming node is the head
            this.head = incomingOnewayNode;
        } else {
            //do addition to beginning of list and shift the head to incoming node
            incomingOnewayNode.next = head;
            this.head = incomingOnewayNode;
        }
    }

    public void deleteHead() {
        this.head = head.next;
    }

    public void display() {
        OnewayNode<T> currOnewayNode = this.head;
        System.out.println();
        System.out.printf("(");
        while (currOnewayNode != null) {
            System.out.printf(currOnewayNode.value + ",");
            currOnewayNode = currOnewayNode.next;
        }
        System.out.printf(")");
    }
}
