package com.heckdevice.dsa.udemy;

/**
 * Created by shailesh on 27/07/17.
 */
public class TwowayNode<T> {
    private T value;
    public TwowayNode<T> next;
    public TwowayNode<T> previous;

    public TwowayNode(T value, TwowayNode<T> next, TwowayNode<T> previous) {
        this.value = value;
        this.next = next;
        this.previous = previous;
    }
}