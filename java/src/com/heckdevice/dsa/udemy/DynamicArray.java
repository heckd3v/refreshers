package com.heckdevice.dsa.udemy;

import java.util.Arrays;

/**
 * Created by shailesh on 23/07/17.
 */
public class DynamicArray<T extends Object> {
    private Object[] dataArray;
    private int currentIndex;

    public DynamicArray(int size) {
        this.currentIndex = 0;
        this.dataArray = new Object[(size == 0) ? 1 : size];
    }

    public int getSize() {
        return this.dataArray.length;
    }

    public T getElement(int index) {
        if (index > getSize() - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) dataArray[index];
    }

    public void put(T item) {
        ensureCapacity(this.currentIndex + 1);
        dataArray[this.currentIndex++] = item;
    }

    void ensureCapacity(int minimumCapacity) {
        int currentCapacity = getSize();
        if (currentCapacity < minimumCapacity) {
            //Most optimal is to do 2 the current size for each minimumcapacity hit
            int desiredCapacity = getSize() * 2;
            dataArray = Arrays.copyOf(dataArray, desiredCapacity);
        }
    }
}
