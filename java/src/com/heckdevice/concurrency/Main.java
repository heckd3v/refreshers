package com.heckdevice.concurrency;

import com.heckdevice.concurrency.RunnableImpl.Runner;

public class Main {

    private static volatile Integer cycle = 0;

    public static void main(String[] args) {
        Runner one = new Runner(1, cycle);
        Runner two = new Runner(2, cycle);
        Runner three = new Runner(3, cycle);
        Runner four = new Runner(4, cycle);
        Thread t1 = new Thread(one);
        Thread t2 = new Thread(two);
        Thread t3 = new Thread(three);
        Thread t4 = new Thread(four);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        while (true) {
            if (Runner.getCount() == 100) {
                one.shutdown();
                two.shutdown();
                three.shutdown();
                four.shutdown();
                System.out.println("Count after cleanup :" + Runner.getCount());
                break;
            }
        }
    }
}
