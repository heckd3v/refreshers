package com.heckdevice.concurrency.RunnableImpl;

/**
 * Created by shailesh on 13/07/17.
 */
public class Runner implements Runnable {
    private boolean running = true;

    private int threadId;
    private static volatile Integer counter;

    public Runner(int threadId, Integer counter) {
        this.threadId = threadId;
        this.counter = counter;
    }

    @Override
    public void run() {
        while (running) {
            System.out.println("Waiting for " + counter++ + " cycle in thread " + threadId);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        this.running = false;
    }

    public static int getCount() {
        return counter.intValue();
    }
}
