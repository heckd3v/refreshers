package com.heckdevice.sorts;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by shailesh on 22/07/17.
 */
public class sorttests {

    @Test
    public void TestNaturalOrder() {
        List<Integer> input = Arrays.asList(0, -2, -5, 1, 5, 9);
        List<Integer> expected = Arrays.asList(-5, -2, 0, 1, 5, 9);
        Collections.sort(input, new sorter<Integer>(false));
        Assert.assertEquals(expected, input);
    }

    @Test
    public void TestReverseNaturalOrder() {
        List<Integer> input = Arrays.asList(0, -2, -5, 1, 5, 9);
        List<Integer> expected = Arrays.asList(9, 5, 1, 0, -2, -5);
        Collections.sort(input, new sorter<Integer>(true));
        Assert.assertEquals(expected, input);
    }

    @Test
    public void TestBubbleSortNaturalOder() {
        List<Integer> input = Arrays.asList(-2, -5, 0, 1, 9, 11);
        List<Integer> expected = Arrays.asList(-5, -2, 0, 1, 9, 11);
        sorter so = new sorter(false);
        so.sort(input, SortType.Bubble);
        Assert.assertEquals(expected, input);
    }

    @Test
    public void TestBubbleSortReverseNaturalOder() {
        List<Integer> input = Arrays.asList(-2, -5, 0, 1, 9, 11);
        List<Integer> expected = Arrays.asList(11, 9, 1, 0, -2, -5);
        sorter so = new sorter(true);
        so.sort(input, SortType.Bubble);
        Assert.assertEquals(expected, input);
    }
}