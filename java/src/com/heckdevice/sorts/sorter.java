package com.heckdevice.sorts;

import java.util.Comparator;
import java.util.List;

/**
 * Created by shailesh on 22/07/17.
 */
enum SortType {
    Bubble, Insert, Heap
}

public final class sorter<T extends Integer> implements Comparator<T> {

    private boolean reverseOrder = false;

    public sorter(boolean reverseOrder) {
        this.reverseOrder = reverseOrder;
    }

    public void sort(List<T> data, SortType sortType) {
        this.reverseOrder = reverseOrder;
        switch (sortType) {
            case Bubble:
                bubbleSort(data);
                break;
        }
    }

    private void bubbleSort(List<T> dataList) {
        boolean swaped = true;
        for (int i = 0; i < dataList.size() - 1; i++) {
            swaped = false;
            for (int j = 0; j < dataList.size() - i - 1; j++) {
                if (reverseOrder) {
                    if (dataList.get(j + 1).intValue() > dataList.get(j).intValue()) {
                        swaped = true;
                        T tmp = dataList.get(j);
                        dataList.set(j, dataList.get(j + 1));
                        dataList.set(j + 1, tmp);
                    }
                } else {
                    if (dataList.get(j + 1).intValue() < dataList.get(j).intValue()) {
                        swaped = true;
                        T tmp = dataList.get(j);
                        dataList.set(j, dataList.get(j + 1));
                        dataList.set(j + 1, tmp);
                    }
                }
            }
            if (!swaped)
                break;
        }
    }

    @Override
    public int compare(T o1, T o2) {
        if (reverseOrder)
            return o2.intValue() - o1.intValue();
        else
            return o1.intValue() - o2.intValue();
    }
}